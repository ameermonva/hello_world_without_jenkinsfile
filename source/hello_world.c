#include <stdio.h>

void create_dummy_test_report()
{
	FILE *fptr;
	
	fptr = fopen("test_report.txt", "w");
	fprintf(fptr, "Test result = Success\n");
	fclose(fptr);
}

int main()
{
	printf("Welcome to Jenkins Hands-on Session\n");
	printf("Testing webhook with patch push event####....\n");
	
	/* Create a dummy test report */
	create_dummy_test_report();
	
	return 0;
}
